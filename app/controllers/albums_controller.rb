class AlbumsController < ApplicationController
  def index
    @albums = Album.order(created_at: :desc).all
  end

  def show
    @album = Album.find(params[:id])
    @tracks = @album.tracks
  end
end
